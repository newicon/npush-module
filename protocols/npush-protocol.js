var nps = require('./../stats/npush-stats');
/**
*  APIs Activity Manager
*  
*  Calcualte API activity and send information if any socket 
*  handle that event.
*  
*  @param object socket
*/
exports.PusherAPIStatsRecordCreate = function() {
	
	return {
		s:{						// summary
			m:0,				// m    -> messages count
			pm:0,				// pm	-> messages count 1 second before
			mps:0,				// mps  -> messages per second
			mpm:0,				// mpm  -> messages per minute		
			mph:0,				// mph  -> messages per hour
			dbrps:0,			// dbr	-> db reads per second
			dbwps:0,			// dbw	-> db writes per second
			dbups:0				// dbu	-> db updates per second
		},
		t:{						// temp history
			mh: new Array(),	// last 60 second history
			mhp: 0,				// last 60 second history pointer
			hh: new Array(),	// last 60 minute history
			hhp: 0				// last 60 minute history pointer
		},
		c:{						// connection
			s:false,			// new list is waiting for send
			a:true				// allow traffic
		},
		n:{						// npush
			as:0,				// active sockets
			n:'n/a'				// client name
		},
		i:{						// instance
			pid:process.pid		// worker PID
		}
	};		
}

/**
*	Create proper msg object using data sent form socket to server.
*	Return object ready to send from server to sockets.
*	
*	@param object msg - message from socket which must be broadcasted
*	@param object socket - message sender socket owner
*	
*	@return object
*/
exports.PusherMakeMessage = function(msg, socket) {
	return getPusherMakeMessage(msg, socket);
}

function getPusherMakeMessage(msg, socket) {
	
	var _time = new Date();
	
	return {
		dataSystem : {
			user_id:    socket.user_id,
			user_name:  socket.nickname, 
			room:       socket.room, 
			channel:    socket.channel,
			time:       parseInt(_time.getTime()),
			client:     socket.client,
			type:       0,						// 0 - room, 1 - channel, 2 - app
			archive:    0,						// 0 - from socket, 1 - from db
			msg_id:		0,						// msg id in db
			source:		0						// 0 - socket, 1 - POST
		},
		dataUser :      msg.data,
		event :         msg.event
	};	
}

/**
*	Create proper msg object using mongo document. Return object
*	ready to send from server to socket.
*	
*	@param object doc - mongoDB document
*	@return object
*/
exports.PusherMakeMessageFromDB = function(doc) {

	return {
		dataSystem : {
			user_id:    doc['user']['id'],
			user_name:  doc['user']['name'], 
			room:       doc['room'], 
			channel:    '',
			time:       doc['date'],
			client:     doc['api_key'],
			type:       0,						// 0 - room, 1 - channel, 2 - app
			archive:    1,						// 0 - from socket, 1 - from db
			msg_id:		0,						// msg id in db
			source:		doc['source']			// 0 - socket, 1 - POST
		},
		dataUser :      doc['message']['data'],
		event :         doc['message']['event']
	};
}

/**
*	Create proper msg object using POST Request. Return object
*	ready to send from server to socket.
*	
*	@param object request - POST data
*	@return object
*/
exports.PusherMakeMessageFromPOST = function(req) {
	// must be implemented
}

/**
*	Updating users activity. Activity is updated for userId
*	
*	@param object socket
*/
exports.PusherUserActivityUpdate = function(worker, socket) {

	var _time = new Date();	

	var _search_json = '{ "user_id" : "' + socket.user_id + '" }';
	var _update_json = '{ "user_id" : "' + socket.user_id + '", "date" : ' + _time.getTime() + ', "date_string" : "' + _time.getTime() + '", \n\
			"rooms./' + socket.client + '/' + socket.channel + '/' + socket.room + '/.date" : "' + _time.getTime() + '",\n\
			"rooms./' + socket.client + '/' + socket.channel + '/' + socket.room + '/.name" : "/' + socket.client + '/' + socket.channel + '/' + socket.room + '/" } ';

	_search = JSON.parse(_search_json);
	_update = JSON.parse(_update_json);

	worker.db.collection('act_' + socket.client).update(_search, {
		$set : _update
	} , {
		upsert : true
	} );

	nps.PusherAPIsActivity(worker, socket, 0, 0, 0, 1);
}

/**
*	Saving message sent by socket to server
*	
*	@param object msg
*	@param string path
*	@param object socket
*	@param function callback
*/
exports.PusherUserMessageSave = function(worker, msg, path, socket, callback) {
	
	var _data = getPusherMakeMessage(msg, socket);

	worker.db.collection('msg_' + socket.handshake.client_room).save({
		"api_key": socket.api_key, 
		"room" : path, 
		"user" : {
			"id" : socket.user_id, 
			"name" : socket.user_name
		} , 
		"date": _data.dataSystem.time, 
		"date_string": _data.dataSystem.time.toString(),
		"message" : msg,
		"client" : socket.handshake.address + ':' + socket.handshake.port,
		"source" : 0,									// 0 - socket, 1 - POST
		"as" : 0										// active sockets
	}, function(err, obj){

		_data.dataSystem.msg_id = obj._id;
		callback.call(this, _data);

		// send activity stats for debuging
		nps.PusherAPIsActivity(worker, socket, 1, 0, 1, 0);
	});
}

/**
*	Saving message sent by POST to server
*	
*	@param object msg
*	@param string path
*	@param object socket
*	@param function callback
*/
exports.PusherUserMessageSaveFromPOST = function(worker, msg, path, req, callback) {
	// waiting for implementing. Needed for POST API.
}

/**
*	Updating Receipt Collection for each delivery confirmation
*	
*	@param object msg - object with message id as string
*	@param object socket
*/
exports.PusherUserMessageRead = function(worker, msg, socket) {
	
	var _time = new Date();	

	var _search_json = '{ "msg_id" : "' + msg.msg_id + '" }';
	var _update_json = '{ "msg_id" : "' + msg.msg_id + '", "users.' + socket.user_id + '.date" : "' + _time.getTime() + '"} ';

	_search = JSON.parse(_search_json);
	_update = JSON.parse(_update_json);

	worker.db.collection('msg_read_' + socket.client).update(_search, {
		$set : _update
	} , {
		upsert : true
	} );

	nps.PusherAPIsActivity(worker, socket, 1, 0, 0, 1);
}


/**
 * Listen for Master Cluster Messages
 * Send Respond for Master requests.
 * 
 * @param object data as message from master process
 */
exports.ModulePusherMessageHandler = function(data, worker, options) {        
	if(data.event == 'global-stats') {
		// DATA RECEIVED FROM MASTER
		// all worker PIDs
		worker.workersInstances = data.data.w;        
		// all worker PIDs
		worker.activeSocketsByWorkers = data.data.i;			
		// data activity from master (all workers)
		worker.activityGlobal = data.data.a;
		// data nicknames from master (all workers)
		worker.nicknameGlobal = data.data.n;
		// global traffic
		worker.allowGlobalTraffic = data.data.t.a;
		// send stats to scokets
		// worker/options are used as GLOBAL VARS
		nps.PusherAPIsActivityStats(worker, worker.io, options);
	} else if(data.event == 'send-activity') {
		// SEND DATA ON MASTER REQUEST
		// data activity from worker to master
		process.send({
			event:'worker-activity', data:{a:worker.activity,n:worker.nicknames}});
		// some action after send stats to master
		for(socket in worker.activity) {
			worker.activity[socket].s.pm = worker.activity[socket].s.m;
			worker.activity[socket].s.dbrps = worker.activity[socket].s.dbwps = worker.activity[socket].s.dbups = 0;		
		}
	}
}