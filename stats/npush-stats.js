var npp = require('./../protocols/npush-protocol'),
	os  = require('os-utils');
		
/**
*	Increase counters of traffic in sockets and database
*	
*	@param object worker
*	@param object socket
*	@param int	  m		- number of messages send in socket
*	@param int	  dbr	- number of db read operations
*	@param int	  dbw	- nubmer of db write operations
*	@param int	  dbu	- number of db update operations
*/
exports.PusherAPIsActivity = function(worker, socket, m, dbr, dbw, dbu) {
	getPusherAPIsActivity(worker, socket, m, dbr, dbw, dbu);
}

function getPusherAPIsActivity(worker, socket, m, dbr, dbw, dbu) {
	// socket.client is equal to API_KEY
	if(!worker.activity[socket.client]) {
		worker.activity[socket.client] = npp.PusherAPIStatsRecordCreate();
	}
	// sockets sounters   
	worker.activity[socket.client].s.m += m;
	// db counters
	worker.activity[socket.client].s.dbrps += dbr;	
	worker.activity[socket.client].s.dbwps += dbw;
	worker.activity[socket.client].s.dbups += dbu;
}

/**
*  Send API Activity for socket on path npush/stats and npush/users
*  with subscribed event for "apiactivity"
*  
*  IMPORTANT CHANNEL/ROOM PATHS:
*  - /CLIENT_API_KEY/npush/api/stats
*  - /CLIENT_API_KEY/npush/api/users
*  - /NPUSH_SERVER_STATUS_APIKEY/npush/system/stats (APIKEY is a option in worker instance)
*  You have to subscribe for one or many of channels to receive stats data.
*  
*  @param object worker
*  @param object io	as instance of socket.io
*  @param object options
*/
exports.PusherAPIsActivityStats = function(worker, io, options) {
	// finish here if worker is not first one
	// ONLY FIRST WORKER IN CLUSTER WILL BROADCAST THAT DATA.
	if(options.id > 1) return;
	// get list of rooms in socket.io
	var rooms = io.sockets.manager.rooms;	
	// calculate statistics
	var pmem = process.memoryUsage();
	var ppid = process.pid;
	// temp object with important info for Administrator
	var sstats = {};										// all sockets stats
	var tstats = {mps:0,mpm:0,mph:0,dbr:0,dbw:0,dbu:0};		// global traffic stats
	var aconn = 0;											// all active connections
	// socket is really API key
	for(socket in worker.activityGlobal) {		
		// if socket (API key) doesn't exist then create empty
		if(!worker.activity[socket]) {
			worker.activity[socket] = npp.PusherAPIStatsRecordCreate();
		}
		// MPS - calculate how many messages was sent last second
		worker.activityGlobal[socket].s.mps = worker.activityGlobal[socket].s.m - worker.activityGlobal[socket].s.pm;
		// MPM - calculate how many messages in last minute
		worker.activity[socket].t.mh[worker.activity[socket].t.mhp] = worker.activityGlobal[socket].s.mps;
		if(worker.activity[socket].t.mhp <  60) {
			worker.activity[socket].t.mhp++
		} else {
			worker.activity[socket].t.mhp = 0;
			worker.activity[socket].t.hhp++				
		}
		var mpm = 0;
		for(i=0; i<worker.activity[socket].t.mh.length;i++) {
			mpm += worker.activity[socket].t.mh[i];
		}
		worker.activity[socket].s.mpm = mpm;			
		// MPH - calculate how many messages in last hour
		worker.activity[socket].t.hh[worker.activity[socket].t.hhp] = worker.activity[socket].s.mpm;

		if(worker.activity[socket].t.hhp >  59) {
			worker.activity[socket].t.hhp = 0;
		}
		var mph = 0;
		for(i=0; i<worker.activity[socket].t.hh.length;i++) {
			mph += worker.activity[socket].t.hh[i];			
		}
		worker.activity[socket].s.mph = mph;
		// send stats to subscribed sockets
		var _path = '/' + socket + '/npush/api/stats';
		worker.activity[socket].s.proc = {mem:pmem.rss, pid:ppid};
		// set info for global stats
		worker.activityGlobal[socket].s.mpm = worker.activity[socket].s.mpm;
		worker.activityGlobal[socket].s.mph = worker.activity[socket].s.mph;
		// SEND CALCULATED "APIACTIVITY" INFO TO USERS
		for(room in rooms) {
			if(room.search(_path) == 0) {
				io.sockets.in(room.substr(1, room.length)).emit('apiactivity', worker.activityGlobal[socket].s);
				getPusherAPIsActivity(worker, {client:socket}, 1, 0, 0, 0);
			}
		}
		// SEND CALCULATED "USERSAPP" INFO TO USERS
		if(worker.activityGlobal[socket].c.s) {
			var _path = '/' + socket + '/npush/api/users';
			for(room in rooms) {
				if(room.search(_path) == 0) {
					io.sockets.in(room.substr(1, room.length)).emit('usersapp', worker.nicknameGlobal[socket]);
					getPusherAPIsActivity(worker, {client:socket}, 1, 0, 0, 0);
				}
			}
			worker.activityGlobal[socket].c.s = false;
		}
		// set admin-stats for this API
		sstats[socket] = {
			mps	:	worker.activityGlobal[socket].s.mps,
			mpm	:	worker.activityGlobal[socket].s.mpm,
			s	:	worker.activityGlobal[socket].n.as,
			n	:	worker.activityGlobal[socket].n.n
		};
		// gobal traffic info
		tstats.mps += worker.activityGlobal[socket].s.mps;
		tstats.mpm += worker.activityGlobal[socket].s.mpm;
		tstats.mph += worker.activityGlobal[socket].s.mph;
		tstats.dbr += worker.activityGlobal[socket].s.dbrps;
		tstats.dbw += worker.activityGlobal[socket].s.dbwps;
		tstats.dbu += worker.activityGlobal[socket].s.dbups;
		// summ all connections
		aconn += worker.activityGlobal[socket].n.as;
	}  	

	// PREPARE STATS FOR DEDICATED OPERATIN SYSTEM
	// THIS IS IMPORTANT: 
	// "NPUSH_SERVER_STATUS_APIKEY":
	//		- you have to set that in option of worker instance on server side
	//		- you have to use that when creating channel on client side
	os.cpuUsage(function(v) {		
		var _path = '/' + options.NPUSH_SERVER_STATUS_APIKEY + '/npush/system/stats';	
		var _data = {
			node: new Array(),
			api:{
				act:sstats
			},
			os:{
				cpu:v,
				tm:os.totalmem(),
				fm:os.freemem(),
				node:{
					i: worker.workersInstances.length,
					s: options.startInstancesNumber
				}
			},
			mongo: new Array(),
			sockets:{
				active:aconn,
				traffic:worker.allowGlobalTraffic,
				nodes:worker.activeSocketsByWorkers
			},
			traffic:tstats
		};
		if(worker.system == 1) { // for Linux
			var command = 'top -n1 -b';
			require('child_process').exec(command, function(error, stdout, stderr) {
				var lines = stdout.split("\n");
				lines.forEach(function(_item,_i) {
					var _p = _item.replace( /[\s\n\r]+/g,' ').split(' ');
					if(String(_p[12]).indexOf('mongod') > -1) {
						_data.mongo.push({pid:_p[1],cpu:_p[9],pmem:_p[6],rss:_p[10],name:_p[12]});
					} else if(_p[0] == ppid) {
						_data.node = {pid:_p[0],cpu:_p[8],pmem:_p[5],rss:_p[9],name:_p[11]};
					} 					
				});
				for(room in rooms) {
					if(room.search(_path) == 0) {
						io.sockets.in(room.substr(1, room.length)).emit('announcement', _data);
						getPusherAPIsActivity(worker, {client:options.NPUSH_SERVER_STATUS_APIKEY}, 1, 0, 0, 0);
					}
				}
			});		
		} else { // for OSX
			var command = 'ps -eo pid,pcpu,pmem,rss,comm';
			require('child_process').exec(command, function(error, stdout, stderr) {				
				var lines = stdout.split("\n");
				lines.forEach(function(_item,_i) {
					var _p = _item.replace( /[\s+\n\r]+/g,' ').split(' ');
					if(String(_p[4]).indexOf('mongod') > -1) {
						_data.mongo.push({pid:_p[0],cpu:_p[1],pmem:_p[2],rss:_p[3],name:_p[4]});
					} else if((parseInt(_p[0]) > 0 && PusherFindInArray(worker.workersInstances ,_p[0])) 
						|| (_p[0] == '' && parseInt(_p[1]) > 0 && PusherFindInArray(worker.workersInstances ,_p[1]))) {
						// we need get list of PIDs in cluster and get data for them
						if(_p[0] == '') {
							_data.node.push({pid:_p[1],cpu:_p[2],pmem:_p[3],rss:_p[4],name:_p[5]});
						} else {
							_data.node.push({pid:_p[0],cpu:_p[1],pmem:_p[2],rss:_p[3],name:_p[4]});
						}
					}                    
				});
				for(room in rooms) {
					if(room.search(_path) == 0) {
						io.sockets.in(room.substr(1, room.length)).emit('announcement', _data);
						getPusherAPIsActivity(worker, {client:options.NPUSH_SERVER_STATUS_APIKEY}, 1, 0, 0, 0);
					}
				}
			});
		}
	});
}

/**
 * Serach value in array
 * @param array           _array
 * @param string|int|bool _value
 * @returns bool
 */
function PusherFindInArray(_array, _value) {
	for(_x in _array) {
		if(_value == _array[_x]) return true;
	}
	return false;
}