<?php
/*
 * BASIC ACTIVITY APPLICATION
 */
?>

<script src="../lib/npush.js" type="text/javascript" rel="stylesheet" /></script>
<link rel="stylesheet" type="text/css" href="styles/chat.css">

<div id="chat">
	<div id="nickname">
			<form id="set-nickname" class="wrap">
					<p>Please type in your nickname and press enter.</p><input id="nick"><p id="nickname-err">Nickname already in use</p>
			</form>
			<div>
				<select id="room">
						<option>room</option>
				</select>
				<select id="channel">
						<option>channel</option>
				</select>
			</div>
	</div>
	<div id="connecting">
			<div class="wrap module-loading-spinner">Connecting to Push Notification Server</div>
	</div>
	<div id="messages" style="background-color: white; height:auto; overflow:auto;">
			<div id="nicknames" style="display:none;"></div>			
			<div id="userschannel" style="display:none;"></div>
            <div id="apiactivity" class="alert alert-info" style="border: 1px #ddd solid; color:#666; background-color: #eee; margin-bottom:6px; margin-left:10px; margin-right: 5px;"><strong>Waiting for traffic information...</strong></div>
			<div id="dbactivity" class="alert alert-info" style="border: 1px #ddd solid; color:#666; background-color: #eee; margin-bottom:6px; margin-left:10px; margin-right: 5px;"><strong>Waiting for database information...</strong></div>
			<div id="sysactivity" class="alert alert-info hide" style="border: 1px #ddd solid; color:#666; background-color: #eee; margin-bottom:6px; margin-left:10px; margin-right: 5px;"><strong>Waiting for system information...</strong></div>
			<div id="usersapplist" class="alert alert-info" style="border: 1px #ddd solid; color:#666; background-color: #eee; margin-bottom:6px; margin-left:10px; margin-right: 5px;"><strong>Waiting for users list...</strong></div>
            <div id="channels" class="alert alert-info" style="margin-bottom:6px; margin-left:10px; margin-right: 5px;"><strong>Waiting for channels list...</strong></div>			
			<div id="lines" style="display:none;"></div>
	</div>
	<div id="history" style="display:none;"></div>
</div>

<script>
	
	var mystatschannel = null;
	var myuserschannel = null;
    
    function ModulePusherSubscribeServiceChannel() {

		npush.init({ 
				'apiKey' : '<?php echo $apiId; ?>',
				'url' : '<?php echo $server['push_address']; ?>', 
				'port' : <?php echo $server['push_port']; ?>, 
				'user_id' : '<?php echo $userId; ?>', 
				'user_name' : '<?php echo $userName; ?>'
			}		
		);		
				
		mystatschannel = npush.subscribe('<?php echo $channel; ?>/<?php echo $roomStats; ?>', {
				'connect' : ModulePusherSubscribeService_connected,
				'connecting' : ModulePusherSubscribeService_connecting,
				'reconnecting' : ModulePusherSubscribeService_reconnecting,
				'disconnect' : ModulePusherSubscribeService_disconnect,
				'error' : ModulePusherSubscribeService_error,
                'apiactivity' : ModulePusherSubscribeService_apiactivity
			} 		
		);
	
		myuserschannel = npush.subscribe('<?php echo $channel; ?>/<?php echo $roomUsersApp; ?>', {
				'connect' : ModulePusherSubscribeService_connected,
				'connecting' : ModulePusherSubscribeService_connecting,
				'reconnecting' : ModulePusherSubscribeService_reconnecting,
				'disconnect' : ModulePusherSubscribeService_disconnect,
				'error' : ModulePusherSubscribeService_error,
				'announcement' : ModulePusherSubscribeService_announcement,
				'usersapp' : ModulePusherSubscribeService_nicknamesapp
			} 		
		);

		/*
		 *	BASIC EXAMPLE
		 
			mychannel = npush.subscribe('mychannel');
			
			mychannel.on('message', function(message) { 

				alert(message); 
			});			
	
			mychannel.trigger('message', 'Hello world!);
		 
		 *	ADVANCED EXAMPLE:

			// init push

			npush.init({
						'apiKey' : 'your_api_key',
						'url' : 'push_server_ip_or_url', 
						'port' : 'push_server_port', 
						'user_id' : 'id_as_number_or_string',	// allow use history
																// as user activity
						'user_name' : 'name_asnumber_or_String'
					});
	
			// create channel
		
			mychannel = npush.subscribe('mychannel');
			mychannel = npush.subscribe('mychannel/myroom');
			mychannel = npush.subscribe('mychannel/myrooms/family');
			mychannel = npush.subscribe('mychannel', { 
														connect : function() {},
														disconnect : function() {},
														error : function() {}
													});
	
			// events
			// event_name is any string
	
			mychannel.on('event_name', function(dataUser) { 

				alert(dataUser); 
			});	
	
			mychannel.on('event_name', function(dataUser, dataSystem) { 

				alert(dataUser + ' ' + dataSystem); 
			});

			// user activity
			// to work with activity you have to provide uniqe USER_ID on init
	
			mychannel.activity(0);		// from last activity on channel/room
			mychannel.activity(60000);	// from last 60 seconds
	
			// send message
	
			mychannel.trigger('event_name', _msg);		// send to api/channel/room/*
			mychannel.channel('event_name', _msg);		// send to api/channel/*
			mychannel.app('event_name', _msg);			// send to api/*
	
			// system events, you can add callback on channel subscribe
				- connect			callback()
				- connecting		callback()
				- connect_fail		callback()
				- reconnect			callback()
				- reconnecting		callback()
				- reconnect_fail	callback()
				- disconnect		callback()
				- error				callback(e)
				- message			callback(dataObject) 
									-> dataObject = { 
												dataUser: StringOrObject, 
												dataSystem: {
													
													client		: String
													room		: String
													channel		: String
													time		: Int
													user_id		: String
													user_name	: String
													type		: Int
													archive		: Int
												}, 
												event : String 
											}
				- announcement		callback(dataString)
				- usersroom			callback(dataArray)
				- userschannel		callback(dataArray)
				- usersapp			callback(dataArray)
		 */
    }    
	
	// npush channel events
    function ModulePusherSubscribeService_connected() {
        $('#chat').addClass('connected');
        $('#chat').addClass('nickname-set'); 
		try {
			window.parent.ModulePusherDashboardStatusConnected();
		} catch(e) {}
    }

    function ModulePusherSubscribeService_connecting() {
		// nothing to do now
    }

    function ModulePusherSubscribeService_announcement(msg) {
		// nothing to do now
    }

    var _max_connection = 0;
    function ModulePusherSubscribeService_nicknamesapp(app) {

		var _string = '';
		var _count = 0;

        $('#channels').empty().html('<strong>Active channels and rooms:</strong>');

		for (var a in app) {
            $('#channels').append('<li>' + a + '</li>');
			for (var r in app[a]) {
                $('#channels').append('<li>' + a + '/' + r + '</li>');
				for (var u in app[a][r]) {
                    //$('#channels').append('<li>' + a + '/' + r + '/' + u + '</li>');    
					_string = _string + '<span class="badge badge-inverse">' + app[a][r][u] + ' : ' + u +'</span>&nbsp;';
					_count++;
				}
			}            
		}
      
		if(_count > _max_connection) _max_connection = _count;
		var _time = new Date();
		$('#usersapplist').empty().append($('<span><strong>Active sockets</strong><br>(now: ' + _count + ', max. in session: ' + _max_connection + '):</span><br>'));
		$('#usersapplist').append(_string);
      
		try {
			  window.parent.ModulePusherDashboardUpdate(_count);            
		} catch(e) {}
    }

    function ModulePusherSubscribeService_disconnect() { 
		try {
			window.parent.ModulePusherDashboardStatusDisconnected();
		} catch(e) { }
    } 

    function ModulePusherSubscribeService_reconnecting() {
		// nothing to do now
    }

    function ModulePusherSubscribeService_error(e) {
		// nothing to do now
    }
    
    function ModulePusherSubscribeService_apiactivity(data) {
        try {
            $('#apiactivity').empty().append($('<span><strong>Traffic meter</strong></span><br><span class="badge badge-inverse">' + data.mps + ' msg/s</span>&nbsp;<span class="badge badge-inverse">' + data.mpm + ' msg/m</span>&nbsp;<span class="badge badge-inverse">' + data.mph + ' msg/h</span>&nbsp;<span class="badge badge-inverse">' + data.m + ' msg/server-running</span>&nbsp;'));
			$('#dbactivity').empty().append($('<span><strong>Database meter</strong></span><br><span class="badge badge-inverse">' + data.dbrps + ' reads/s</span>&nbsp;<span class="badge badge-inverse">' + data.dbwps + ' writes/s</span>&nbsp;<span class="badge badge-inverse">' + data.dbups + ' updates/s</span>&nbsp;'));
            window.parent.ModulePusherDashboardUpdateSpeedMeterValue(data);
        } catch(e) {}
    }
    
    // dom manipulation
    $(document).ready(function() {
		viewUpdate();
		ModulePusherSubscribeServiceChannel();
		$(window).resize(function() {
			viewUpdate();
		});
    });     
    
    // refreshing view on resize
	function viewUpdate() {
		$('#messages').height($(window).height()-110);
	}
	
	function roundNumber(num, dec) {
		var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
		return result;
	}	
</script>