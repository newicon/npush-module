/**
 *	NEWICON NPUSH SERVER MODULE
 *
 *	@author	Krzysztof Stasiak <chris.stasiak@newicon.net>
 *	@name	index.js
 *
 *	Copyright(c) 2013 NEWICON LTD <newicon@newicon.net>
 *	GPL Licensed
 *	
 */

exports.createMaster = require('./npush-master').createMaster;
exports.createWorker = require('./npush-worker').createWorker;