var npp  = require('./../protocols/npush-protocol'),
	nps  = require('./../stats/npush-stats');
/**
 * 
 * @param {socket.io} io
 * @param object worker
 */
exports.createNPushSocketIOEngine = function(io, db, worker) {
	/**
	* Handle event for each socket. This 'connection' event function is called 
	* when handshake proccess return true.
	* 
	* Events:
	* - message
	* - message-read
	* - message boradcast rooms
	* - message broadcast app
	* - message broadcast channel
	* - public_activity_ms
	* - public_activity
	* - disconnect
	* 
	*/
	io.sockets.on('connection', function (socket) {
		// USER LOGIN AND ENVIRONMENT SETTINGS
		// SEND WELCOME MESSAGE
		io.sockets.socket(socket.id).emit('announcement', socket.handshake.client_wellcome_msg);
		// SET USER SOCKET PARAMETERS
		socket.api_key = socket.handshake.api_key;		// THIS IS API KEY
		socket.channel = socket.handshake.channel;
		socket.client = socket.handshake.client_room;	// THIS IS API KEY
		socket.path = socket.handshake.client_room + '/' + socket.handshake.channel + '/' + socket.handshake.user_room;
		socket.room = socket.handshake.user_room;
		socket.user_id = socket.handshake.user_id;
		socket.user_name = socket.handshake.user_name;
		// ADD USER TO LIST
		if(!worker.nicknames[socket.handshake.client_room]) {	    
			worker.nicknames[socket.handshake.client_room] = {};
		}
		if(!worker.activity[socket.handshake.client_room]) {
			worker.activity[socket.handshake.client_room] = npp.PusherAPIStatsRecordCreate();
		}
		if(!worker.nicknames[socket.handshake.client_room][socket.handshake.channel]) {
			worker.nicknames[socket.handshake.client_room][socket.handshake.channel] = {};
		}
		if(!worker.nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room]) {
			worker.nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room] = {};		
		}
		worker.nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room][socket.handshake.user_id] = socket.nickname = socket.handshake.user_name;
		// JOIN ROOM
		socket.join(socket.path);
		// set info in activity object - list will be send to watchers	
		worker.activity[socket.client].c.s = true;
		// update connected user in socket -> as is active-sockets
		worker.activity[socket.client].n.as++;
		// USER MESSAGES
		socket.on('message', function (_msg) {

			if(!worker.allowGlobalTraffic) return;

			// set path to room
			var _path = '/' + socket.handshake.client_room + '/' + socket.channel + '/' + socket.room + '/';        

			// save message in db
			npp.PusherUserMessageSave(worker, _msg, _path, socket, function(_data) { 

				// send message
				io.sockets.in(socket.path).emit('message', _data);
			});
		});
		socket.on('message-read', function (_msg) {

			npp.PusherUserActivityUpdate(worker, socket);		
			npp.PusherUserMessageRead(worker, _msg, socket);
		});
		// BROADCAST MESSAGES
		socket.on('message boradcast rooms', function (_msg, fn) {

			if(!worker.allowGlobalTraffic) return;

			var _path = '/' + socket.client + '/' + socket.channel + '/';

			// save message in db
			npp.PusherUserMessageSave(worker, _msg, _path, socket, function(_data) { 

				// broadcast to app
				_data.dataSystem.type = 1;      

				// get all rooms
				var rooms = io.sockets.manager.rooms;        

				for(room in rooms)
					if(room.search(_path) == 0)
						io.sockets.in(room.substr(1, room.length)).emit('message', _data);
			});
		});
		socket.on('message broadcast app', function (_msg, fn) {

			if(!worker.allowGlobalTraffic) return;

			var _path = '/' + socket.client + '/';

			// save message in db
			npp.PusherUserMessageSave(worker, _msg, _path, socket, function(_data) { 

				// broadcast to app
				_data.dataSystem.type = 3;

				// get all rooms
				var rooms = io.sockets.manager.rooms;

				for(room in rooms)
					if(room.search(_path) == 0)
						io.sockets.in(room.substr(1, room.length)).emit('message', _data);	
			});
		});  
		socket.on('message broadcast channel', function (_channel, _msg, fn) {

			if(!worker.allowGlobalTraffic) return;

			var _path = '/' + socket.client + '/' + _channel + '/';

			// save message in db
			npp.PusherUserMessageSave(worker, _msg, _path, socket, function(_data) { 

				// broadcast to app
				_data.dataSystem.type = 2;

				// get all rooms
				var rooms = io.sockets.manager.rooms; 

				for(room in rooms)
					if(room.search(_path) == 0)
						io.sockets.in(room.substr(1, room.length)).emit('message', _data);
			});
		});  
		// READ TIME OF LAST ACTIVITY IN MS FROM DB AND SEND UNREAD MESSAGES
		socket.on('public_activity_ms', function (_period, fn) {

			if(!worker.allowGlobalTraffic) return;

			var _search_time = ' { "user_id" : "' + socket.user_id + '", "rooms./' + socket.client + '/' + socket.channel + '/' + socket.room + '/.date" : {"$exists": true} } ';

			_search = JSON.parse(_search_time);

			db.collection('act_' + socket.handshake.client_room).find(_search).forEach(function(err, doc) {

				if (!doc) return;

				var _time = new Date();
				socket.user_last_activity = _time.getTime() - _period;

				var _time_info = new Date(socket.user_last_activity*1);

				// send info
				io.sockets.socket(socket.id).emit('announcement', 'activity since ' + _time_info + ' on ' 
					+ '/' + socket.client + '/' + socket.channel + '/' + socket.room + '/');

				// SEARCH FOR MESSAGES FOR ROOM
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/' + socket.channel + '/' + socket.room + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.collection('msg_' + socket.handshake.client_room).find(_search).forEach(function(err, doc) {

					if (!doc) return;

					io.sockets.socket(socket.id).emit('message', npp.PusherMakeMessageFromDB(doc));

					nps.PusherAPIsActivity(worker, socket, 1, 1, 0, 0);
				});

				// SEARCH FOR MESSAGES FOR CHANNEL
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/' + socket.channel + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.collection('msg_' + socket.handshake.client_room).find(_search).forEach(function(err, doc) {

					if (!doc) return;

					io.sockets.socket(socket.id).emit('message', npp.PusherMakeMessageFromDB(doc));

					nps.PusherAPIsActivity(worker, socket, 1, 1, 0, 0);
				});

				// SEARCH FOR MESSAGES FOR APP
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.collection('msg_' + socket.handshake.client_room).find(_search).forEach(function(err, doc) {

					if (!doc) return;

					io.sockets.socket(socket.id).emit('message', npp.PusherMakeMessageFromDB(doc));

					nps.PusherAPIsActivity(worker, socket, 1, 1, 0, 0);
				});		
			});
		});  	
		// READ TIME OF LAST ACTIVITY FROM DB AND SEND UNREAD MESSAGES
		socket.on('public_activity', function (nick, fn) {

			if(!worker.allowGlobalTraffic) return;

			var _search_time = ' { "user_id" : "' + socket.user_id + '", "rooms./' + socket.client + '/' + socket.channel + '/' + socket.room + '/.date" : {"$exists": true} } ';

			search = JSON.parse(_search_time);

			db.collection('act_' + socket.handshake.client_room).find(search).forEach(function(err, doc) {

				if (!doc) return;

				socket.user_last_activity = doc['rooms']['/' + socket.client + '/' + socket.channel + '/' + socket.room + '/']['date'];
				var _time = new Date(socket.user_last_activity*1);

				// send info
				io.sockets.socket(socket.id).emit('announcement', 'last activity ' + _time + ' on ' 
					+ '/' + socket.client + '/' + socket.channel + '/' + socket.room + '/');

				// SEARCH FOR MESSAGES FOR ROOM
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/' + socket.channel + '/' + socket.room + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.collection('msg_' + socket.handshake.client_room).find(_search).forEach(function(err, doc) {

					if (!doc) return;

					io.sockets.socket(socket.id).emit('message', npp.PusherMakeMessageFromDB(doc));

					nps.PusherAPIsActivity(worker, socket, 1, 1, 0, 0);
				});

				// SEARCH FOR MESSAGES FOR CHANNEL
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/' + socket.channel + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.collection('msg_' + socket.handshake.client_room).find(_search).forEach(function(err, doc) {

					if (!doc) return;

					io.sockets.socket(socket.id).emit('message', npp.PusherMakeMessageFromDB(doc));

					nps.PusherAPIsActivity(worker, socket, 1, 1, 0, 0);
				});

				// SEARCH FOR MESSAGES FOR APP
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.collection('msg_' + socket.handshake.client_room).find(_search).forEach(function(err, doc) {

					if (!doc) return;

					io.sockets.socket(socket.id).emit('message', npp.PusherMakeMessageFromDB(doc));

					nps.PusherAPIsActivity(worker, socket, 1, 1, 0, 0);
				});
			});
		});
		// SERVER CONTROLL COMMAND
		socket.on('system', function(data) {
			process.send({
				event:'system-command', data:data});
		});
		// DISCONNECT AND SAVE TIME OF EVENT IN DB
		socket.on('disconnect', function () {
			
			// delete user from list
			if (!socket.nickname) return;

			npp.PusherUserActivityUpdate(worker, socket);

			try {
				// delete user_id from array    
				delete worker.nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room][socket.user_id];

				// delete room space in array if no user exist
				if(Object.keys(worker.nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room]).length == 0)
					delete worker.nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room];

				// delete channe; space in array if no channel exist
				if(Object.keys(worker.nicknames[socket.handshake.client_room][socket.handshake.channel]).length == 0)
					delete worker.nicknames[socket.handshake.client_room][socket.handshake.channel];

				// delete channe; space in array if no channel exist
				if(Object.keys(worker.nicknames[socket.handshake.client_room]).length == 0) {

					delete worker.nicknames[socket.handshake.client_room];
					delete worker.activity[socket.handshake.client_room];

				} else {

					// set info in activity object - list will be send to watchers
					worker.activity[socket.client].c.s = true;		
					// update connected user in socket -> as is active-sockets
					worker.activity[socket.client].n.as--;						
				}
			} catch(e) {
				console.log('error on disconnect: ' + e);
			}
		});
	});		
}