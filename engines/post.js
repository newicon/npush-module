var npp  = require('./../protocols/npush-protocol'),
	nps  = require('./../stats/npush-stats');

/*******************************************************************************
* POST REQUESTS
* NEED OPTIMIZATION IN VERSION 3.1
* 
* TO DO !!!!!!!!!!!!!!!!!!!!!!
* - implement universal function to save/update for POST API INTERFACE
* 
******************************************************************************/

exports.createNPushHttpPostEngine = function(app, io, db, worker) {

	app.post('/api/channel/:c/room/:r/api/:k/version/:v/t/:t/suid/:suid/suname/:sun', function (req, res) {

		// :c - destination channel
		// :r - destination room
		// :k - api key
		// :v - api version
		// :t - time stamp
		// :suid - sender user id
		// :sun - sender user name

		// EXAMPLE:
		// curl -H "Content-Type: application/post" 
		//		-d '{"object":"helloworld"}' 
		//		"http://push.vm.newicon.net:80/api/channel/ch1/room/room1/api/ab3245cbeaf456244abcdfa/version/2.0/t/234324/suid/1/suname/me"


		PusherSendMessageFromPOST(db, worker, req, res, 0, function(status, data, path) {

			if(status) {

				if(req.params.r != '') {

					io.sockets.in(path).emit('message', data);						
				}

				res.send('OK');

			} else {

				res.send('AUTH_ERROR');
			}
		});
	});		

	app.post('/api/channel/:c/api/:k/version/:v/t/:t/suid/:suid/suname/:sun', function (req, res) {

		// :c - destination channel
		// :r - destination room
		// :k - api key
		// :v - api version
		// :t - time stamp
		// :suid - sender user id
		// :sun - sender user name

		// EXAMPLE:
		// curl -H "Content-Type: application/post" 
		//		-d '{"object":"helloworld"}' 
		//		"http://push.vm.newicon.net:80/api/channel/ch1/api/ab3245cbeaf456244abcdfa/version/2.0/t/234324/suid/1/suname/me"

		if(req.method == 'POST' && worker.allowGlobalTraffic) {

			// DATA

			var body ='';
			var _time_server = new Date();

			req.on('data', function (data) {
				body += data;
			});		

			req.on('end', function () {

				var _search_msg = ' { "key" : "' + req.params.k + '", "enabled" : "true" } ';
				_search = JSON.parse(_search_msg);			

				db.collection('apis').find(_search).forEach(function(err, doc) {			

					if (!doc) {

						res.send('AUTH_ERROR');
						return;
					} else {

						var rooms = io.sockets.manager.rooms;
						var _path = '';

						try {
							_msg_object = JSON.parse(body);
						} 
						catch (e) {
							_msg_object = {data : body, event : 'message'}
						}

						try {

							_time_stamp = parseInt(req.params.t);
						} catch(e) {

							_time_stamp = req.params.t;
						}					

						var data = {
							dataSystem : {
								client:req.params.k,
								user_id:req.params.suid,
								user_name:req.params.sun, 
								room:'', 
								channel:req.params.c,
								time:Math.floor(_time_server.getTime()),
								type:2,
								archive:0,
								msg_id:0,
								source:1
							},
							dataUser : _msg_object.data,
							event : _msg_object.event
						}

						if(req.params.k != '')
							_path = req.params.k;
						if(req.params.c != '')
							_path = _path + '/' + req.params.c;

						db.collection('msg_'+req.params.k).save({

							"api_key": req.params.k, 
							"room" : '/' + _path + '/', 
							"user" : {
								"id" : req.params.suid, 
								"name" : req.params.sun
							} , 
							"date": _time_server.getTime(), 
							"date_string": _time_server.getTime().toString(), 
							"message" : _msg_object,
							"client" : req.connection.remoteAddress + ':' + req.connection.remotePort,
							"source" : 1,
							"as" : 0

						}, function(err, obj) {

							data.dataSystem.msg_id = obj._id;

							npp.PusherUserActivityUpdate(worker, data.dataSystem);

							if(req.params.c != '') {

								for(room in rooms) {

									if(room.search('/'+_path + '/') == 0) {
										io.sockets.in(room.substr(1, room.length)).emit('message', data);		
									}
								}
							}

							// update API Activity Stats
							nps.PusherAPIsActivity(worker, {client:req.params.k}, 1, 1, 1, 0);                        

							res.send('OK');
							return;						
						});
					}
				});				
			});
		} else {

			res.send('TRAFFIC_STOPPED');
			return;			
		}
	});

	// POST FOR APP/

	app.post('/api/api/:k/version/:v/t/:t/suid/:suid/suname/:sun', function (req, res) {

		// :c - destination channel
		// :r - destination room
		// :k - api key
		// :v - api version
		// :t - time stamp
		// :suid - sender user id
		// :sun - sender user name

		// EXAMPLE:
		// curl -H "Content-Type: application/post" 
		//		-d '{"object":"helloworld"}' 
		//		"http://push.vm.newicon.net:80/api/api/ab3245cbeaf456244abcdfa/version/2.0/t/234324/suid/1/suname/me"

		if(req.method == 'POST' && worker.allowGlobalTraffic) {

			// DATA

			var body ='';
			var _time_server = new Date();

			req.on('data', function (data) {
				body += data;
			});		

			req.on('end', function () {

				var _search_msg = ' { "key" : "' + req.params.k + '", "enabled" : "true" } ';
				_search = JSON.parse(_search_msg);			

				db.collection('apis').find(_search).forEach(function(err, doc) {			

					if (!doc) {

						res.send('AUTH_ERROR');
						return;
					} else {

						var rooms = io.sockets.manager.rooms;
						var _path = '';

						try {
							_msg_object = JSON.parse(body);
						} 
						catch (e) {
							_msg_object = {data : body, event : 'message'}
						}

						try {
							_time_stamp = parseInt(req.params.t);
						} catch(e) {

							_time_stamp = req.params.t;
						}					

						var data = {
							dataSystem : {
								client:req.params.k,
								user_id:req.params.suid,
								user_name:req.params.sun, 
								room:'', 
								channel:'',
								time:Math.floor(_time_server.getTime()),
								type:3,
								archive:0,
								msg_id:0,
								source:1
							},
							dataUser : _msg_object.data,
							event : _msg_object.event
						}

						if(req.params.k != '')
							_path = req.params.k;

						db.collection('msg_'+req.params.k).save({
							"api_key": req.params.k, 
							"room" : '/' + _path + '/', 
							"user" : {
								"id" : req.params.suid, 
								"name" : req.params.sun
							} , 
							"date": _time_server.getTime(), 
							"date_string": _time_server.getTime().toString(), 
							"message" : _msg_object,
							"client" : req.connection.remoteAddress + ':' + req.connection.remotePort,
							"source" : 1,
							"as" : 0

						}, function(err, obj) {

							data.dataSystem.msg_id = obj._id;	

							npp.PusherUserActivityUpdate(worker, data.dataSystem);

							if(req.params.k != '') {

								for(room in rooms) {

									if(room.search('/'+_path + '/') == 0)
									{
										io.sockets.in(room.substr(1, room.length)).emit('message', data);
									}
								}					
							}

							// update API Activity Stats
							nps.PusherAPIsActivity(worker, {client:req.params.k}, 1, 1, 1, 0);

							res.send('OK');
							return;
						});
					}
				});				
			});
		} else {
			res.send('TRAFFIC_STOPPED');
			return;			
		}
	});
}

function PusherSendMessageFromPOST(db, worker, req, res, type, callback) {

	if(req.method == 'POST' && worker.allowGlobalTraffic) {
		// DATA		
		var body ='';
		var _time_server = new Date();

		req.on('data', function (data) {
			body += data;
		});		

		req.on('end', function () {
			var _auth_ok = false;
			var _search_msg = ' { "key" : "' + req.params.k + '", "enabled" : "true" } ';
			_search = JSON.parse(_search_msg); 

			db.collection('apis').find(_search).forEach(function(err, doc) {			
				if (!doc) {
					if(!_auth_ok) {
						res.send('AUTH_ERROR');
					}
					return;
				} else {					
					var _path = '';
					// important because we use asynchronus callbacks
					_auth_ok = true;
					try {
						_msg_object = JSON.parse(body);
					} catch (e) {
						_msg_object = {data : body, event : 'message'}
					}
					try {
						_time_stamp = parseInt(req.params.t);
					} catch(e) {
						_time_stamp = req.params.t;
					}					

					var data = {
						dataSystem : {
							client:req.params.k,
							user_id:req.params.suid,
							user_name:req.params.sun, 
							room:req.params.r,					// becarefull here
							channel:req.params.c,				// becarefull here
							time:Math.floor(_time_server.getTime()),
							type:type,							// becarefull here
							archive:0,
							msg_id:0,
							source:1
						},
						dataUser : _msg_object.data,
						event : _msg_object.event
					}

					if(req.params.k != '')
						_path = req.params.k;
					if(req.params.c != '')
						_path = _path + '/' + req.params.c;		// becarefull here
					if(req.params.r != '')
						_path = _path + '/' + req.params.r		// becarefull here

					db.collection('msg_'+req.params.k).save({
						"api_key": req.params.k, 
						"room" : '/' + _path + '/', 
						"user" : {
							"id" : req.params.suid, 
							"name" : req.params.sun
						} , 
						"date": _time_server.getTime(), 
						"date_string": _time_server.getTime().toString(), 
						"message" : _msg_object,
						"client" : req.connection.remoteAddress + ':' + req.connection.remotePort,
						"source" : 1,
						"as" : 0

					}, function(err, obj) {

						data.dataSystem.msg_id = obj._id;									

						npp.PusherUserActivityUpdate(worker, data.dataSystem);

						// update API Activity Stats
						nps.PusherAPIsActivity(worker, {client:req.params.k}, 1, 1, 1, 0);                        

						callback.call(this, true, data, _path);
					});
				}
			});				
		});
	} else {
		res.send('TRAFFIC_STOPPED');
		return;			
	}
}